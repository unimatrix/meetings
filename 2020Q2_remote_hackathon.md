# 2020 Q2 Remote Hackathon Planning

* Theme: Hybrid solutions.
* Date: June 16 - June 18, 2020
* Time: 10:00 - 17:00 UTC

## Recordings

* Day 1 morning: <https://youtu.be/TaRSlOkZ0A4>
* Day 1 afternoon: <https://youtu.be/3wAMuyeXY8s>
* Day 3: <https://youtu.be/-usLNdmGykk>

## Day 1: Presentations

Join using [Teams](https://teams.microsoft.com/l/meetup-join/19%3ameeting_NmMyODMyYmYtZDIyNS00OTRkLThkMWMtYWJjN2U4ZWJiYmVk%40thread.v2/0?context=%7b%22Tid%22%3a%2278703d3c-b907-432f-b066-88f7af9ca3af%22%2c%22Oid%22%3a%22cb806734-eca6-46eb-8a4c-79412972d6c9%22%7d). All sessions will be recorded and published under [Meetings](https://gitlab.com/unimatrix/meetings).

1. 10:00 [Welcome](Hackathon_2020Q2/101-UniMatrix_Hackathon_intro.pdf), Per Björkdahl, Axis Communications
1. 10:15 [Goals for 2020](Hackathon_2020Q2/102-UniMatrix_goals_2020.pdf), Fredrik Svensson, Axis Communications
1. 10:45 [Grand design & architecture](Hackathon_2020Q2/103-UniMatrix_GrandDesign_20200616_r9.pdf), Yusuke Ogata, Panasonic i-PRO
1. 11:15 RAM & flash budget estimation
    1. [Mikael Lindberg, Axis Communications](Hackathon_2020Q2/104a-Unimatrix%20RAM%20and%20Flash%20budget.pdf)
    1. [Takahiro Yamaguchi, Panasonic i-PRO](Hackathon_2020Q2/104b-UniMatrix_HWRes_Target_20200615c.xlsx)
    1. Huatao Qin, Hikvision
1. 12:00 Video API
    1. [Zero-copy with OpenCV capture](Hackathon_2020Q2/105-Zero%20Copy%20.pdf), Hussan Munir, Axis Communications
1. 12:30 Inference API
    1. [Larod inference engine update](Hackathon_2020Q2/106a-unimatrix_larod-20200616.pdf), Jon Eibertzon, Axis Communications
    1. Inference Framework API, Renguang Feng, Hikvision
    1. [MindSpore presentation](Hackathon_2020Q2/106c-MindSpore%20Unimatrix%20Hackathon.pdf), Howard Huang, Huawei
1. 14:00 Capability API
    1. [API technologies](Hackathon_2020Q2/107-Unimatrix_APIs_20200616.pptx), Dora Han, Hikvision
1. 14:30 Metadata API
    1. [Metadata in protobuf format](Hackathon_2020Q2/108-Unimatrix%20Metadata%20in%20Protobuf%20format.pdf), Sriram Bhetanabottla, Axis Communications
1. 15:00 Orchestration
    1. KubeEdge update, Kevin Wang, Huawei
1. 15:30 [CI/CD and testing strategies](Hackathon_2020Q2/110-UNIMATRIX%20--%20CI_CD-tech.pdf), Brian Rossa, F0cal
1. 16:00 Technical Board meeting, see [agenda](TB/20200616_mom.md).
1. 17:00 Demo of VS Code Live Share and VS Codespaces, Fredrik Svensson, Axis Communications

## Day 2: Hackathon

### Preparations

1. Install Visual Studio Code, <https://code.visualstudio.com/>
1. Install the Live Share extension[^vscodelive] for VS Code
1. Install the Live Share Audio extension for VS Code
1. Join the slack channel, <https://unimatrix-workspace.slack.com/>
    1. Join link: <https://join.slack.com/t/unimatrix-workspace/shared_invite/zt-f0cb0m5w-pguAV6Z_EvO_qcFVJnbZpw>

### Hacking

Each task team has a lead and team members. There is no specific time set for the hacking sessions. Each team can use whatever time schedule that suits them best. Synchronize working hours on the slack #general channel.

1. The lead starts a collaboration session in VS Code. (Live Share -> "Start collaboration session").
1. The lead publishes the collaboration URI in the slack #general channel.
1. Other team members join the collaboration session (Live Share -> "Join collaboration session").
1. You should now be able to share code, console and audio. Happy hacking! :)
1. The lead prepares a presentation of the results for the 3rd day.

### Tasks

1. Implement capabilities API. Compare JSON vs gRPC. (Hikvision, Videotec)
1. Two stage video analytics on different tiers. (Axis, Genetec)
    1. Edge device does object classification and produces metadata including bounding box.
    1. Video and metadata is pushed from edge to a recording service using DASH Ingest.
    1. The recording service stores CMAF fragments in Azure Blob Storage.
    1. Cloud does further analytics, e.g. recognition, in a serverless function.
1. Panasonic i-PRO
    * Implement 2 Inference Client Apps on KubeEdge Pod.
        * One of the Client Apps will run on RasPi4, and another on PC.
        * One of the Client Apps will be for backup.
    * Implement an Inference User App on KubeEdge Pod, which runs on another PC.
1. Metadata over RTP (Panasonic i-PRO)

## Day 3: Presentation of tasks

Join using [Teams](https://teams.microsoft.com/l/meetup-join/19%3ameeting_NmMyODMyYmYtZDIyNS00OTRkLThkMWMtYWJjN2U4ZWJiYmVk%40thread.v2/0?context=%7b%22Tid%22%3a%2278703d3c-b907-432f-b066-88f7af9ca3af%22%2c%22Oid%22%3a%22cb806734-eca6-46eb-8a4c-79412972d6c9%22%7d). All sessions will be recorded and published under [Meetings](https://gitlab.com/unimatrix/meetings).

## Tools

### Tele conference

We will use [Microsoft Teams](https://www.microsoft.com/en/microsoft-365/microsoft-teams/download-app) for the presentations and discussions.

### Chat

We will use Slack for chatting, please join the [UniMatrix Slack channel](https://unimatrix-workspace.slack.com/)

* Join link: <https://join.slack.com/t/unimatrix-workspace/shared_invite/zt-f0cb0m5w-pguAV6Z_EvO_qcFVJnbZpw>

### Live code sharing

We will use [Visual Studio Code](https://code.visualstudio.com/) with Live Share and Live Share Audio extensions[^vscodelive] for the hacking sessions.

## Notes

[^vscodelive]: You need an authenticated github or Microsoft account to share.
