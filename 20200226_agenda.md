# Telco 2020-02-26

## Agenda
1. Governance update
   * Governance documents
   * LF startup
1. ONVIF update
   * Open up spec work
   * Open meetings, e.g. "dev summit"
   * Cloud/Hybrid Profile
   * Alignment of APIs betyween UniMatrix and OSSA
1. Hackathon update
   * Corona virus
   * Coordinate with ONVIF "dev summit"?
   * Use-case to collaborate on?
1. AOB
   * Next telco
