# Container Security Recommendations

1. Automatically scan containers for known vulnerabilities. This is provided by multiple registries, e.g. Docker Hub using Snyk.
1. Reduce attack surface by minimizing _runtime containers_**, e.g. by using docker-slim.
1. Use hardening flags when compiling and linking containers, see e.g. [Debian Hardening guide](https://wiki.debian.org/Hardening)
1. Generate security profiles for secccomp and App Armor for _runtime containers_.
1. Use the user namespace to remap container's root to something else on host.
1. Create individual users per app.
1. An App's privileges should be controlled through a manifest and verified at installation. This should give the App access to needed user groups, files and APIs.
1. Signing of Apps should be done by UniMatrix and included in the App's Manifest to be verified upon installation.

** By _runtime containers_ we mean containers that will run on the target device, i.e. not SDK containers.

See this guide for more information about container security,
<https://i.blackhat.com/eu-18/Wed-Dec-5/eu-18-Benameur-Container-Attack-Surface-Reduction-Beyond-Name-Space-Isolation.pdf>
