# Git Structure

Proposal to reorganize the structure of the Git repository.

* governance
* meetings
    * GB
    * TB
    * CB
* spec
* sdk
    * OpenCV
    * tfserving
    * models
* examples
    * object-detector
    * object-detector-cpp
    * object-detector-larod
* containers
    * dbus
    * larod
    * larod-inference-server
* larod
* larod-inference-server
* reference
    * raspberry-pi
