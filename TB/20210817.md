# Technical Board meeting 2021-08-17

Recording: <https://youtu.be/ZgVYNNx2MLw>

## Agenda

1. OpenCV required feature set. Which libraries and features should be enabled?
1. Device security discussion
   - Provisioning/secure on-boarding of new devices
   - Certificate distribution
   - User management
   - LSMs
   - etc.
1. AOB

After the call please fill in the questionnaire: <https://forms.office.com/r/SBzvhZbQuw>

## Minutes / Action points

1. **Axis, Hikvision, i-PRO** Write down security recommendations for next call.
1. **Axis, Hikvision, i-PRO** Invite security expert to join next call
1. Next call will be August 31st, 13:00 UTC
