# UniMatrix December 2019 Hackathon

* Please login to gitlab read the presentations.
* To be able read the presentations you need to be a member of the UniMatrix group.

## Participants

 1. Fredrik Svensson, Axis Communications
 1. Willy Sagefalk, Axis Communications
 1. Per Björkdahl, Axis Communications
 1. Mikael Lindberg, Axis Communications
 1. Tove Bergdahl, Axis Communications
 1. Jon Eibertzon, Axis Communications
 1. Markus Skans, Axis Communications
 1. Johan Gunnarsson, Axis Communications
 1. Anders Mårtensson, Axis Communications
 1. Linh Trang, Axis Communications
 1. Erik Jansson, Axis Communications
 1. Jiandan Chen, Axis Communications
 1. Andy O Truong, Axis Communications
 1. Daniel Larsson, Axis Communications
 1. Hussan Munir, Axis Communications
 1. Dora Han, Hikvision
 1. Adler Wu, Hikvision
 1. Ye Tingqun, Hikvision
 1. Leo Qin, Hikvision
 1. Paul Chen, Hikvision
 1. Hirokazu Kitaoka, Panasonic i-PRO Sensing Solutions
 1. Takahiro Yamaguchi, Panasonic i-PRO Sensing Solutions
 1. Yasuo Yomogida, Panasonic i-PRO Sensing Solutions
 1. Kaoru Tsurumi, Panasonic i-PRO Sensing Solutions
 1. Khanh, Panasonic i-PRO Sensing Solutions
 1. Kun, Panasonic i-PRO Sensing Solutions
 1. Hong Hung, Panasonic i-PRO Sensing Solutions
 1. Ottavio Campana, Videotec
 1. Manuel Turetta, Videotec
 1. Sean Wang, Futurewei
 1. Judith Zhang, Huawei
 1. Kevin Wang, Huawei

## Tuesday, December 17

 1. [Purpose and goal of UniMatrix](https://gitlab.com/unimatrix/private/raw/master/Hackathon_201912/101-UniMatrix_Intro_20191217.pptx), Per Björkdahl, Axis Communications
 2. [Container tech introduction](https://gitlab.com/unimatrix/private/raw/master/Hackathon_201912/102-Unimatrix%20container%20talk.pdf), Mikael Lindberg, Axis Communications
 3. [UniMatrix high level overview](https://gitlab.com/unimatrix/private/raw/master/Hackathon_201912/103-UniMatrix_overview.pptx), Fredrik Svensson, Axis Communications
 4. [Standardizing APIs in UniMatrix](https://gitlab.com/unimatrix/private/raw/master/Hackathon_201912/104-Unimatrix_standardize_APIs_Sweden_201912.pptx), Dora Han, Hikvision
 5. [UniMatrix Scope](https://gitlab.com/unimatrix/private/raw/master/Hackathon_201912/105-Unimatrix%20Review%20of%20Panasonic_final.pptx), Yasuo Yomogida, Panasonic i-PRO Sensing Solutions
 6. [Service Interface from Huawei SDC](https://gitlab.com/unimatrix/private/raw/master/Hackathon_201912/106-Service%20Interface%20from%20Huawei%20SDC.PPTX), Judith Zhang, Huawei
 7. [Hackathon Demo](https://gitlab.com/unimatrix/private/raw/master/Hackathon_201912/107-Hackathon_Demo.pptx), Erik Jansson & Jiandan Chen, Axis Communications

## Wednesday, December 18

 1. [Larod Inference Engine](https://gitlab.com/unimatrix/private/raw/master/Hackathon_201912/201-inference.pptx), Jon Eibertzon, Axis Communications
 2. [DNN Model Conversion](https://gitlab.com/unimatrix/private/raw/master/Hackathon_201912/202-model_conversion.pptx), Jon Eibertzon, Axis Communications
 3. [Video Capture API](https://gitlab.com/unimatrix/private/raw/master/Hackathon_201912/203-hackathonVideoAPI.pdf), Linh Trang, Axis Communications
 4. [KubeEdge presentation](https://gitlab.com/unimatrix/private/raw/master/Hackathon_201912/204-KubeEdge-intro-for-Unimatrix-hackathon.pdf), Kevin Wang, Huawei
 5. [KubeEdge presentation video](https://gitlab.com/unimatrix/private/raw/master/Hackathon_201912/205-Kevin_Wang-KubeEdge-presentation.mp4), Kevin Wang, Huawei

## Thursday, December 19

 1. [Panasonic MQTT Demo](https://gitlab.com/unimatrix/private/raw/master/Hackathon_201912/301-Unimatrix_Hackathon_MQTT_Panasonic_20191219f.pptx), Takahiro Yamaguchi, Panasonic i-PRO Sensing Solutions
