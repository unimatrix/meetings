# 2021 Q1 Virtual Hackathon

* Date: January 26 - January 28, 2021
* Time: 10:00-17:00 UTC
* [Teams join link](https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZTlhNTEyNjUtN2ZkNS00MDQzLTlmYjktMWVkZWE4YWNlNTI4%40thread.v2/0?context=%7b%22Tid%22%3a%2278703d3c-b907-432f-b066-88f7af9ca3af%22%2c%22Oid%22%3a%22cb806734-eca6-46eb-8a4c-79412972d6c9%22%7d)
* [Slack join link](https://join.slack.com/t/unimatrix-workspace/shared_invite/zt-l2c0ujth-~7eeTOzDJcpbGrsvwN1jGA)

## Day 1: Presentations and discussions

1. 10:00 Welcome and GB update, Per Björkdahl
1. 10:30 Larod inference API update, Jon Eibertzon
1. 11:00 Break
1. 11:15 Inference server with TensorFlow Serving API, Johan Ahlkvist
1. 11:30 Tutorial C++ example using inference server, Johan Ahlkvist

## Day 2: Hackathon

### Preparations

1. Install Visual Studio Code, https://code.visualstudio.com/
1. Install the Live Share extension for VS Code
1. Install the Live Share Audio extension for VS Code
1. Join the slack channel
    * Join link: https://join.slack.com/t/unimatrix-workspace/shared_invite/zt-l2c0ujth-~7eeTOzDJcpbGrsvwN1jGA
    * Slack channel, https://unimatrix-workspace.slack.com/

## Hackathon Tasks

1. 10:00, Larod Native Example, Jon Eibertzon, Axis

### Larod Native Example

Develop a complete example of a containerized application using OpenCV for image capture and larod as inference engine. The application implements a complete pipeline including image capture, image pre-processing, inference and eventual result post-processing.

* Pre-requisites
    * Docker
    * OpenCV/v4l2
    * Larod
    * A DL model, e.g. MobileNetV2 or MobileNetV2 SSD
* Questions
    * What could be done in order to make the workflow more efficient?

## Day 3: Hackathon

1. xx:xx Continue Hackathon teams at their preferred time.
1. 14:00 Team leads present results for each task.

## Platform

This is the suggested platform to use during the Hackathon.

1. Generic Linux PC with docker and docker-compose
1. V4L2 camera
1. nVidia GPU
1. UniMatrix base layer
    1. unimatrix/base for C/C++ projects
    1. unimatrix/base-python for Python projects

## Tools

### Tele conference

We will use [Microsoft Teams](https://www.microsoft.com/en/microsoft-365/microsoft-teams/download-app) for the presentations and discussions.

### Chat

We will use Slack for chatting, please join the [UniMatrix Slack channel](https://unimatrix-workspace.slack.com/).

### Live code sharing

We will use [Visual Studio Code](https://code.visualstudio.com/) with Live Share and Live Share Audio extensions[^vscodelive] for the hacking sessions.

## Notes

[^vscodelive]: You need an authenticated github or Microsoft account to share.
