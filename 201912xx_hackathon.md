Hackathon Planning
==================
* Meeting type: Hackathon
* Location: Sankt Gertrud, Östergatan 7B, 211 25 Malmö, https://www.sanktgertrud.se/
* Time: 2019-12-17 @ 8:30 - 2019-12-19 @ 17:00

Draft Agenda
------------
### Tuesday, December 17
#### Common track
* 08:30 Welcome and introductions, Per Björkdahl
* 08:45 Purpose and goal of UniMatrix, Per Björkdahl
* 09:00 Containers and trends, technical overview, Mikael Lindberg
* 09:45 Coffee break
* 10:15 UniMatrix, high level overview, Fredrik Svensson
* 10:45 Architecture overview discussion, Fredrik Svensson & Mikael Lindberg
    1. Requirements and use-cases
    1. Axis proposal
    1. Hikvision proposal
* 12:30 Lunch

#### Technical track
* 13:30 Proof-of-concept demo, Jiandan Chen & Erik Jansson
* 14:30 Hands on exercises, Mikael Lindberg
* 15:30 Coffee
* 16:00 Hands on exercises

#### Governance track
* 13:30 Legal aspects
* 15:30 Coffee
* 16:00 Governance discussion

### Wednesday, December 18
#### Common track
* 08:30 N.N. model conversion discussion, Jon Eibertzon & Markus Skans
    1. Requirements
    1. Axis proposal
    1. Hikvision proposal
* 09:45 Coffee
* 10:15 Inference API discussion, Jon Eibertzon & Markus Skans
    1. Requirements
    1. Axis proposal
    1. Hikvision proposal
* 11:30 Video API discussion, Linh Trang
    1. Requirements
    1. OpenCV proposal
    1. OpenCV vendor backends
* 12:30 Lunch
* 13:30 Orchestration discussion
    1. KubeEdge presentation, Huawei
* 14:30 Next meeting planning

#### Technical track
* 15:00 Identify collaboration tasks
* 15:30 Coffee
* 16:00 Hackathon
* 19:00 Dinner

#### Governance track
* 15:00 When/how to go public
* 15:30 Coffee
* 16:00 Governance discussion
* 19:00 Dinner

### Thursday, December 19
* 08:30 Hackathon
* 12:30 Lunch
* 13:30 Hackathon
* 16:30 Summarize and present

Collaboration task examples
---------------------------
1. Port video analytics/deep learning examples to the reference platform
    1. YOLOv3
1. Evaluate different model servers, e.g. Redis AI and Tengine
1. Evaluate backends for OpenCV
1. Evaluate orchestration runtimes, e.g. KubeEdge and K3S

Target platform
---------------
Bring your own target platform to work on:
* [Raspberry Pi 4](https://www.raspberrypi.org/products/raspberry-pi-4-model-b/)
* [Raspberry Pi camera module v2](https://www.raspberrypi.org/products/camera-module-v2/)
* [5.1V / 3.0A DC power supply](https://www.raspberrypi.org/products/type-c-power-supply/)
* [64 GB SD card, e.g. SanDisk Extreme Pro](https://www.sandisk.com/home/memory-cards/sd-cards/extremepro-sd-uhs-i-90mbps)
* [Raspbian Buster with desktop and recommended software](https://www.raspberrypi.org/downloads/raspbian/)

Development environment
-----------------------
* The Raspberry Pi will be used as development platform.
* There will be a number of HD monitors, USB keyboards and USB mice available to connect to the Pi.
* You can also connect to the Pi using ssh or realvnc to your laptop.
* There will be wired and wireless networking available.

Development SDK
---------------
The following components will be provided by a base container layer:
* OpenCV
* TensorFlow Lite
* C/C++
* Python
